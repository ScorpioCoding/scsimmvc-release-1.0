<?php

/**
 *  cBackend
 */
class Backend extends Controller
{
	
	public function __construct()
	{
		parent::__construct();
	}

	public function dashboard()
	{
        $this->view->render('3', 'dashboard');
	}

	public function login()
	{
			$this->view->render('3', 'login');
	}

	public function logout()
	{
			$this->view->render('3', 'logout');
	}

	public function project()
	{
			$this->view->render('3', 'project');
	}

	public function whatevercomesnext()
	{
			$this->view->render('3', 'whatevercomesnext');
	}
}