<?php
/**
*  Base Controller
*/
class Controller extends Functions
{
	protected $params = array();

	public function __construct()
    {
		parent::__construct();
		$this->view = new View();
	}

	// public setParams( $params = array() )
	// {
	// 	$this->params = $params;
	// }







	/*
	* Loads the Model
	* @params string $model
	*/
	protected function model($model)
	{
		$file = PATH_MODEL . $model .'.php';
		if ( file_exist( $file ) )
		{ 
			require($file);
			return new $model();
		}		
	}
	

	protected function translation($translation)
	{
		//a method to acces / bind the translation to a view/ Model variables
		//eg.    in the template header the title
		// trans.en.php  en  - title = english
		// trans.nl.php  nl  - title = nederlands

		//these translation files can be outsourced

		//these translations are for our core elements and not dynamic centent...
		//like buttons, links, headers, fixed content,

		// the translation array is then sent to the view where the arry is extracted into seperate variables
		// which can be found in the tempplates.

		// $file = PATH_MODEL . $model .'.php';
		// if ( file_exist( $file ) )
		// { 
		// 	require($file);
		// 	return new $model();
		// }		
	}

}//END CLASS
?>
