<?php

/**
*  Base View
*/
class View extends Functions
{
	public function __construct()
    {
		parent::__construct();
	}

	/*
	* render the view
	* @params int 		$renderOption  (0,1,2) 0: noInclude, 1: frontend, 2 backend,
	* @params string 	$name	
	* @params array 	$data
	*/
	public function render($renderOption = 1, $name, $data = array() )
	{
		$paths = array (
			1 => 
				array ( 1 =>  PATH_F_VIEW . $name . '.phtml'),
			2 =>
				array ( 1 => PATH_F_VIEW_TMP . 'header.phtml',
						2 => PATH_F_VIEW . $name . '.phtml',
						3 => PATH_F_VIEW_TMP . 'footer.phtml'),
			3 =>
				array ( 1 => PATH_B_VIEW_TMP . 'header.phtml',
						2 => PATH_B_VIEW . $name . '.phtml' ,
						3 => PATH_B_VIEW_TMP . 'footer.phtml'
			));

		
		self::renderPage($renderOption, $paths, $data );

			
	}

} //END CLASS
?>

